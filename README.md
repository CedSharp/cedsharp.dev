# CedSharp.dev

Source code for CedSharp's blog hosted at [https://cedsharp.dev](https://cedsharp.dev).

## Technologies

The static website is built using Astro, fetching data from PayloadCMS.
The frontend uses Astro's templating language in addition to Scss.
